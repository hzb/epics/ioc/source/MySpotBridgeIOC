#!../../bin/linux-x86_64/MySpotBridge

< envPaths
epicsEnvSet("ENGINEER","Luca Porzio")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/MySpotBridge.dbd"
MySpotBridge_registerRecordDeviceDriver pdbbase

## Load record instances

#var streamDebug 1

cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > /opt/epics/ioc/log/logs.dbl
